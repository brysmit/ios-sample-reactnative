#!/usr/bin/env bash
if [ "$APPCENTER_BRANCH" == "master" ];
then
    echo "Current branch is master"
else
    echo "Current branch is $APPCENTER_BRANCH"
fi
